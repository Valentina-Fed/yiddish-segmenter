# -*- coding: utf-8 -*-
import argparse
import re
import os
from pathlib import Path

def get_argparser():
  parser = argparse.ArgumentParser()
  parser.add_argument("--input_file", type=str, default='./data/input.txt',
                      help="path to input file")
  parser.add_argument("--output_file", type=str, default='./data/output.txt',
                      help="path to output file")
  
  return parser

alphabets= "(\\w)"
prefixes = "(Mr|St|Mrs|Ms|Dr)[.]"
suffixes = "(Inc|Ltd|Jr|Sr|Co)"
starters = "(Mr|Mrs|Ms|Dr|He\s|She\s|It\s|They\s|Their\s|Our\s|We\s|But\s|However\s|That\s|This\s|Wherever)"
acronyms = "(\\w[.]\\w[.](?:\\w[.])?)"
websites = "[.](com|net|org|io|gov)"

def split_into_sentences(text):
  text = " " + text + "  "
  text = text.replace("\n"," ")
  text = re.sub(prefixes,"\\1<prd>",text)
  text = re.sub(websites,"<prd>\\1",text)
  if "Ph.D" in text: text = text.replace("Ph.D.","Ph<prd>D<prd>")
  text = re.sub("\s" + alphabets + "[.] "," \\1<prd> ",text)
  text = re.sub(acronyms+" "+starters,"\\1<stop> \\2",text)
  text = re.sub(alphabets + "[.]" + alphabets + "[.]" + alphabets + "[.]","\\1<prd>\\2<prd>\\3<prd>",text)
  text = re.sub(alphabets + "[.]" + alphabets + "[.]","\\1<prd>\\2<prd>",text)
  text = re.sub(" "+suffixes+"[.] "+starters," \\1<stop> \\2",text)
  text = re.sub(" "+suffixes+"[.]"," \\1<prd>",text)
  text = re.sub(" " + alphabets + "[.]"," \\1<prd>",text)
  if "”" in text: text = text.replace(".”","”.")
  if "\"" in text: text = text.replace(".\"","\".")
  if "!" in text: text = text.replace("!\"","\"!")
  if "?" in text: text = text.replace("?\"","\"?")
  text = text.replace(".",".<stop>")
  text = text.replace("?","?<stop>")
  text = text.replace("!","!<stop>")
  text = text.replace("…","…<stop>")
  text = text.replace("<prd>",".")
  sentences = text.split("<stop>")
  sentences = sentences[:-1]
  sentences = [s.strip() for s in sentences]
  return sentences

def main():
  opts = get_argparser().parse_args()

  Path(opts.output_file).parent.mkdir(parents=True, exist_ok=True)
  
  with open(opts.output_file, "w") as output:
    with open(opts.input_file) as input:
      i = 0
      for line in input:
        i = i+1
        line = line.replace('-', '־')
        line = line.replace("‛", "'")
        line = line.replace('“', '"')
        line = line.replace('„', '"')
        line = line.replace('...', '…')
        sentences = split_into_sentences(line)
        j = 0
        for sentence in sentences:
          if (sentence):
            j = j+1
            sentence = sentence.strip()
            print(f'sentence: {sentence}')
            output.write('\n')
            output.write(f'#File: {os.path.basename(opts.input_file)}\n')
            output.write(f'#Line: {i}, Sentence: {j}\n')
            output.write(f'#Sentence: {sentence}\n')
            words = sentence.split()
            for word in words:
              print(f'initial: {word}')
              prefixes = []
              while re.fullmatch('^\\W.+', word):
                prefixes.append(word[:1])
                word = word[1:]
              
              if re.fullmatch("^[סרמכ]'.*", word):
                prefixes.append(word[:2])
                word = word[2:]

              suffixes = []
              while re.fullmatch('.+(?![ֲִַָֹּ])\\W$', word):
                suffixes.append(word[-1:])
                word = word[:-1]
              
              for prefix in prefixes:
                print(f'prefix: {prefix}')
                output.write(f'{prefix}\n')

              if len(word) > 0:
                print(f'word: {word}')
                output.write(f'{word}\n')
              
              for suffix in suffixes[::-1]:
                print(f'suffix: {suffix}')
                output.write(f'{suffix}\n')

if __name__ == '__main__':
  main()
